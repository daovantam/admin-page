
import React, { Component } from "react";
import { Layout, Menu, Avatar, Dropdown } from 'antd';
import { connect } from 'react-redux'
import { withRouter } from "react-router";
import AuthActions from '../redux/_auth-redux'
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  DashboardOutlined,
  HomeOutlined,
  ClusterOutlined,
  FileImageOutlined,
  PoweroffOutlined,
  CaretDownFilled
} from '@ant-design/icons';
import queryString from "query-string";

const { Header, Sider, Content, Footer } = Layout;

class LayoutAdmin extends Component {
  state = {
    collapsed: false,
    activeMenuKey: '/'
  };

  componentDidMount() {
    if (window.localStorage.getItem("jwt_auth_token") && !this.props.userInfo.name) {
      this.props.getUserInfor()
    }
    if (!window.localStorage.getItem("jwt_auth_token")) {
      this.props.history.push('/login')
    }
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  toHomePage = () => {
    this.props.history.push("/");
  }

  toUserPage = () => {
    this.props.history.push("/user");
  }

  toBannerPage = () => {
    this.props.history.push("/banner");
  }

  toManageHousePage = () => {
    this.props.history.push("/house");
  }

  toManageHouseCategoryPage = () => {
    this.props.history.push("/house-category");
  }

  handleLogout = () => {
    window.localStorage.clear()
    this.props.history.push('/login')
  }

  render() {
    const { userInfo } = this.props

    const menuDropdown = (
      <Menu>
        <Menu.Item key="1">
          <div className="d-flex-align-center">
            <PoweroffOutlined className="fs-16 mr-2" />
            <a onClick={this.handleLogout}>Đăng xuất</a>
          </div>
        </Menu.Item>
      </Menu>)
    return (
      <div >
        <Layout className="layout_admin">
          <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
            <div className="logo d-flex-center">
              {this.state.collapsed ? <img style={{ height: '42px' }} src={require('../assets/images/logo.jpeg')} /> : <p className="title">House Rental</p>}
            </div>
            <Menu theme="light" mode="inline" defaultSelectedKeys={[this.props.history.location.pathname]} className="fs-16">
              <Menu.Item key="/" className="mt-0" icon={<DashboardOutlined className="fs-16" />} onClick={this.toHomePage}>
                Tổng quan
            </Menu.Item>
              <Menu.Item key="/user" icon={<UserOutlined className="fs-16" />} onClick={this.toUserPage}>
                Người dùng
            </Menu.Item>
              <Menu.Item key="/house" icon={<HomeOutlined className="fs-16" />} onClick={this.toManageHousePage}>
                Quản lý phòng
            </Menu.Item>
              <Menu.Item key="/house-category" icon={<ClusterOutlined className="fs-16" />} onClick={this.toManageHouseCategoryPage}>
                Quản lý loại phòng
            </Menu.Item>
              <Menu.Item key="/banner" icon={<FileImageOutlined className="fs-16" />} onClick={this.toBannerPage}>
                Banner
            </Menu.Item>
            </Menu>
          </Sider>
          <Layout className="site-layout">
            <Header className="site-layout-background d-flex-space-between" style={{ padding: 0 }}>
              {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                className: 'trigger',
                onClick: this.toggle,
              })}
              <ul className="mr-3 mb-0  admin_avatar">
                <li className="d-flex-center">
                  <Avatar style={{ backgroundColor: '#87d068' }} icon={<UserOutlined />} />
                  <Dropdown
                    style={{ width: "500px" }}
                    overlay={menuDropdown}
                    trigger={["click"]}
                  >
                    <a className="ml-1">
                      <span className="fs-14 fw-6">{userInfo?.name}</span> <CaretDownFilled />
                    </a>
                  </Dropdown>
                </li>
              </ul>
            </Header>
            <Content
              className="site-layout-background"
              style={{
                // margin: '24px 16px',
                // padding: 24,
                background: 'none',
              }}
            >
              <div 
              style={{
                margin: '24px 16px 16px 16px',
                padding: 21,
                minHeight: 'calc(100% - 72px)',
                background: '#fff'
              }}
              >
                {this.props.children}
              </div>
              <Footer style={{ textAlign: 'center', padding: '0px 50px 10px 50px' }}>House Rental ©2020</Footer>
            </Content>
            
          </Layout>
        </Layout>

      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    userInfo: state.auth.userInfo
  }
}

const mapDispatchToProps = dispatch => ({
  getUserInfor: () => dispatch(AuthActions.getUserInfoRequest()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(LayoutAdmin))
