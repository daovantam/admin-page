import React, { Component } from 'react';
import './App.css';
import Route from "./router/route";
import { Provider } from "react-redux";
import configureStore from "./redux/index";
import "antd/dist/antd.css";
import './assets/style.scss'
class App extends Component {
  render() {
    return (
      <div>
        <Provider store={configureStore()}>
          <Route />
        </Provider>
      </div>
    );
  }
}

export default App;
