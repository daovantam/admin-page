import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { connect } from "react-redux";

//admin
import LayoutAdmin from "../layout/LayoutAdmin";
import DashBoard from "../pages/DashBoard";
import Login from "../pages/Login"
import User from "../pages/User";
import ManageHouse from "../pages/House/ManageHouse";
import HouseDetail from "../pages/House/HouseDetail";
import ManageHouseCategory from "../pages/HouseCategory/ManageHouseCategory";
import AddHouseCategory from "../pages/HouseCategory/AddHouseCategory";
import EditHouseCategory from "../pages/HouseCategory/EditHouseCategory";
import ManageBanner from "../pages/Banner/ManageBanner";
import AddBanner from "../pages/Banner/AddBanner";
import NotFound from '../pages/Notfound'

class PageLayout extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/login" component={Login} exact />
          <Route>
            <LayoutAdmin>
              <Switch>
                <Route path="/" component={DashBoard} exact />
              </Switch>
              <Switch>
                <Route path="/user" component={User} exact />
              </Switch>
              <Switch>
                <Route path="/house" component={ManageHouse} exact />
              </Switch>
              <Switch>
                <Route path="/house-detail/:id" component={HouseDetail} exact />
              </Switch>
              <Switch>
                <Route path="/house-category" component={ManageHouseCategory} exact />
              </Switch>
              <Switch>
                <Route path="/house-category/:id" component={EditHouseCategory} exact />
              </Switch>
              <Switch>
                <Route path="/add-house-category" component={AddHouseCategory} exact />
              </Switch>
              <Switch>
                <Route path="/banner" component={ManageBanner} exact />
              </Switch>
              <Switch>
                <Route path="/add-banner" component={AddBanner} exact />
              </Switch>
            </LayoutAdmin>
          </Route>
          <Route path="*" component={NotFound} exact />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default PageLayout;