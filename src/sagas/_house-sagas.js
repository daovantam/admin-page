import { put, call } from 'redux-saga/effects'
import axios from 'axios'
import HouseActions from '../redux/_house-redux'
import Notify from '../components/Notification'
import http from '../api/api'

let token = window.localStorage.getItem("jwt_auth_token")
const HouseSagas = {
  *getListHouse(data) {
    try {
      const listHouse = yield call(async () => {
        return await http.post("/manage/api/v1/house/search",
        data.data,
        {
          headers: {
            "Authorization": 'bearer ' + token
          },
        });
      });
      if (listHouse.status < 400) {
        yield put(HouseActions.getListHouseSuccess(listHouse.data));
      } else {
        yield put(HouseActions.getListHouseFailure(listHouse.data));
      }
    } catch (error) {
      yield put(HouseActions.getListHouseFailure(error));
    }
  },

  *getDetailHouse(data) {
    try {
      const detailHouse = yield call(async () => {
        return await http.get(`/manage/api/v1/house/${data.data.id}`,
        {
          headers: {
            "Authorization": 'bearer ' + token
          },
        });
      });
      if (detailHouse.status < 400) {
        yield put(HouseActions.getDetailHouseSuccess(detailHouse.data));
        yield data.data.callback(true);
      } else {
        yield put(HouseActions.getDetailHouseFailure(detailHouse.data));
      }
    } catch (error) {
      console.log(error)
      yield put(HouseActions.getDetailHouseFailure(error));
    }
  },


  *deleteHouse(data) {
    try {
      const deleteHouse = yield call(async () => {
        return await http.delete(`/manage/api/v1/house/${data.data.paramsDelete.id}`,
        {
          headers: {
            "Authorization": 'bearer ' + token
          },
        });
      });
      if (deleteHouse.status < 400) {
        yield put(HouseActions.deleteHouseSuccess(deleteHouse.data));
        yield data.data.callback(true);
        Notify('success', 'Xoá phòng thành công')
      } else {
        yield put(HouseActions.deleteHouseFailure(deleteHouse.data));
      }
    } catch (error) {
      yield put(HouseActions.deleteHouseFailure(error));
    }
  },

}

export default HouseSagas