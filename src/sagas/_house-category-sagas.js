import { put, call } from 'redux-saga/effects'
import axios from 'axios'
// import { commonService } from '../services'
import CategoryActions from '../redux/_category-redux'
import Notify from '../components/Notification'
import http from '../api/api'

let token = window.localStorage.getItem("jwt_auth_token")
const HouseCategorySagas = {
  *getHouseCategory() {
    try {
      const houseCategory = yield call(async () => {
        return await http.get("/manage/api/v1/category", {
          headers: {
            "Authorization": 'bearer ' + token
          },
        });
      });
      if (houseCategory.status < 400) {
        yield put(CategoryActions.getHouseCategorySuccess(houseCategory.data));
      } else {
        yield put(CategoryActions.getHouseCategoryFailure(houseCategory.data));
      }
    } catch (error) {
      yield put(CategoryActions.getHouseCategoryFailure(error));
    }
  },

  *getDetailCategory(data) {
    try {
      const detailCategory = yield call(async () => {
        return await http.get(`/manage/api/v1/category/${data.data.id}`, {
          headers: {
            "Authorization": 'bearer ' + token
          },
        });
      });
      if (detailCategory.status < 400) {
        yield put(CategoryActions.getDetailCategorySuccess(detailCategory.data));
        yield data.data.callback(true);
      } else {
        yield put(CategoryActions.getDetailCategoryFailure(detailCategory.data));
      }
    } catch (error) {
      yield put(CategoryActions.getDetailCategoryFailure(error));
    }
  },

  *addHouseCategory(data) {
    try {
      const addCategory = yield call(async () => {
        return await http.post("/manage/api/v1/category",
        data.data.formData,
        {
          headers: {
            "Authorization": 'bearer ' + token
          },
        });
      });
      if (addCategory.status < 400) {
        yield put(CategoryActions.addHouseCategorySuccess(addCategory.data));
        yield data.data.callback(true);
        Notify('success', 'Thêm mới loại phòng thành công')
      } else {
        yield put(CategoryActions.addHouseCategoryFailure(addCategory.data));
      }
    } catch (error) {
      yield put(CategoryActions.addHouseCategoryFailure(error));
    }
  },

  *deleteHouseCategory(data) {
    try {
      const deleteCategory = yield call(async () => {
        return await http.delete(`/manage/api/v1/category/${data.data.params.id}`,
        {
          headers: {
            "Authorization": 'bearer ' + token
          },
        });
      });
      if (deleteCategory.status < 400) {
        yield put(CategoryActions.deleteHouseCategorySuccess(deleteCategory.data));
        yield data.data.callback(true);
        Notify('success', 'Xoá loại phòng thành công')
      } else {
        yield put(CategoryActions.deleteHouseCategoryFailure(deleteCategory.data));
      }
    } catch (error) {
      console.log(error)
      yield put(CategoryActions.deleteHouseCategoryFailure(error));
    }
  },

  *updateHouseCategory(data) {
    try {
      const updateCategory = yield call(async () => {
        return await http.put("/manage/api/v1/category",
        data.data.params,
        {
          headers: {
            "Authorization": 'bearer ' + token
          },
        });
      });
      console.log(updateCategory)
      if (updateCategory.status < 400) {
        yield put(CategoryActions.updateHouseCategorySuccess(updateCategory.data));
        Notify('success', 'Cập nhật thành công')
        yield data.data.callback(true);
      } else {
        console.log(updateCategory)
        yield put(CategoryActions.updateHouseCategoryFailure(updateCategory.data));
      }
    } catch (error) {
      console.log(error)
      yield put(CategoryActions.updateHouseCategoryFailure(error));
    }
  },
}

export default HouseCategorySagas