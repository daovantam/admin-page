import { takeLatest, all } from "redux-saga/effects";

/* ------------- Types ------------- */
import { AuthTypes } from '../redux/_auth-redux'
import { BannerTypes } from '../redux/_banner-redux'
import { HouseCategoryTypes } from '../redux/_category-redux'
import { HouseTypes } from '../redux/_house-redux'
/* ------------- Sagas ------------- */
import AuthSagas from './_auth-sagas'
import BannerSagas from './_banner-sagas'
import HouseCategorySagas from './_house-category-sagas'
import HouseSagas from './_house-sagas'
/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
    //login
    takeLatest(AuthTypes.LOGIN_REQUEST, AuthSagas.login),
    takeLatest(AuthTypes.GET_USER_INFO_REQUEST, AuthSagas.getUserInfo),

    //banner
    takeLatest(BannerTypes.GET_IMAGE_SLIDE_REQUEST, BannerSagas.getImageSlide),
    takeLatest(BannerTypes.CREATE_IMAGE_SLIDE_REQUEST, BannerSagas.createImageSlide),
    takeLatest(BannerTypes.CHANGE_ACTIVE_SLIDE_REQUEST, BannerSagas.changeActiveSlide),

    //category
    takeLatest(HouseCategoryTypes.GET_HOUSE_CATEGORY_REQUEST, HouseCategorySagas.getHouseCategory),
    takeLatest(HouseCategoryTypes.GET_DETAIL_CATEGORY_REQUEST, HouseCategorySagas.getDetailCategory),
    takeLatest(HouseCategoryTypes.ADD_HOUSE_CATEGORY_REQUEST, HouseCategorySagas.addHouseCategory),
    takeLatest(HouseCategoryTypes.DELETE_HOUSE_CATEGORY_REQUEST, HouseCategorySagas.deleteHouseCategory),
    takeLatest(HouseCategoryTypes.UPDATE_HOUSE_CATEGORY_REQUEST, HouseCategorySagas.updateHouseCategory),

    //house
    takeLatest(HouseTypes.GET_LIST_HOUSE_REQUEST, HouseSagas.getListHouse),
    takeLatest(HouseTypes.GET_DETAIL_HOUSE_REQUEST, HouseSagas.getDetailHouse),
    takeLatest(HouseTypes.DELETE_HOUSE_REQUEST, HouseSagas.deleteHouse),
  ]);
}
