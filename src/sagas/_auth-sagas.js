import { put, call } from 'redux-saga/effects'
import axios from 'axios'
import AuthActions from '../redux/_auth-redux'
import Notify from '../components/Notification'
import {Auth} from '../constants'
import http from '../api/api'

let token = window.localStorage.getItem("jwt_auth_token")
const AuthSagas = {
  *login(data) {
    try {
      const login = yield call(async () => {
        return await http.post("/uaa/oauth/token",
        data.data,
        {
          headers: {
            "Authorization": "Basic " + btoa(Auth.USER_NAME + ":" + Auth.PASSWORD)
          },
        });
      });
      if (login.status < 400) {
        login.data.loginSuccess = true
        window.localStorage.setItem('jwt_auth_token', login.data.access_token)
        yield put(AuthActions.loginSuccess(login.data));
      } else {
        yield put(AuthActions.loginFailure(login.data));
      }
    } catch (error) {
      yield put(AuthActions.loginFailure(error));
    }
  },

  *getUserInfo() {
    try {
      const userInfor = yield call(async () => {
        return await http.get("/uaa/user/current",
        {
          headers: {
            "Authorization": 'bearer ' + token
          },
        });
      });
      if (userInfor.status < 400) {
        yield put(AuthActions.getUserInfoSuccess(userInfor.data));
      } else {
        yield put(AuthActions.getUserInfoFailure(userInfor.data));
      }
    } catch (error) {
      yield put(AuthActions.getUserInfoFailure(error));
    }
  },
}

export default AuthSagas