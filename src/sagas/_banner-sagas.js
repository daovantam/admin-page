import { put, call } from 'redux-saga/effects'
import axios from 'axios'
// import { commonService } from '../services'
import BannerActions from '../redux/_banner-redux'
import Notify from '../components/Notification'
import http from '../api/api'

let token = window.localStorage.getItem("jwt_auth_token")
const BannerSagas = {
  *getImageSlide() {
    try {
      const imageSlide = yield call(async () => {
        return await http.get("/info/api/v1/slide");
      });
      if (imageSlide.status < 400) {
        yield put(BannerActions.getImageSlideSuccess(imageSlide.data));
      } else {
        yield put(BannerActions.getImageSlideFailure(imageSlide.data));
      }
    } catch (error) {
      yield put(BannerActions.getImageSlideFailure(error));
    }
  },

  *createImageSlide(data) {
    try {
      const createSlide = yield call(async () => {
        return await http.post("/info/api/v1/slide",
        data.data.formData, 
        {
          headers: {
            "Authorization": 'bearer ' + token
          },
        });
      });
      if (createSlide.status < 400) {
        yield put(BannerActions.createImageSlideSuccess(createSlide.data));
        yield data.data.callback(true);
        Notify('success', 'Thêm mới banner thành công')
      } else {
        yield put(BannerActions.createImageSlideFailure(createSlide.data));
      }
    } catch (error) {
      console.log(error)
      yield put(BannerActions.getImageSlideFailure(error));
    }
  },

  *changeActiveSlide(data) {
    console.log(data)
    try {
      const changeActiveSlide = yield call(async () => {
        return await http.put("/info/api/v1/slide/change-status",
        data.data.params, 
        {
          headers: {
            "Content-Type": "application/json",
            "Authorization": 'bearer ' + token
          },
        });
      });
      if (changeActiveSlide.status < 400) {
        yield put(BannerActions.createImageSlideSuccess(changeActiveSlide.data));
        yield data.data.callback(true);
        Notify('success', 'Thay đổi trạng thái thành công')
      } else {
        yield put(BannerActions.createImageSlideFailure(changeActiveSlide.data));
      }
    } catch (error) {
      console.log(error)
      yield put(BannerActions.getImageSlideFailure(error));
    }
  },
}

export default BannerSagas