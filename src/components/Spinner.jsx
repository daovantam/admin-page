import React, { Component } from 'react'
import { css } from "@emotion/core";
import BeatLoader from "react-spinners/BeatLoader";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class Spinner extends Component {
  render() {
    return (
      <div className="spinner_loader">
        <BeatLoader
          css={override}
          size={15}
          color={"#ffbd30"}
          loading={true}
        />
        <style>
          {
            `
              .spinner_loader {
                position: fixed;
                width: 100%;
                left: 0;
                right: 0;
                top: 0;
                bottom: 0;
                background-color: rgba(255,255,255,0.7);
                display: flex;
                z-index: 9999;
                align-items: center;
            }`
          }
        </style>
      
      </div>
    )
  }
}
export default Spinner
