import React, { Component } from "react";
import { connect } from 'react-redux'
import { withRouter } from "react-router";
import HouseCategoryActions from '../redux/_category-redux'
import { Switch, Button, Form, Input, Upload, message, Space } from 'antd';
import { CloseOutlined, CheckOutlined, PlusOutlined, LoadingOutlined } from '@ant-design/icons';
import Spinner from '../components/Spinner'

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}
class HouseCategoryForm extends Component {
  formRef = React.createRef();
  state = {
    loading: false,
    activeImage: false
  }

  componentDidMount() {
    this.setState({
      imageUrl: this.props.detailData?.imagePath
    })
  }

  componentDidUpdate() {
    
  }

  addHouseCategory = async (values) => {
    let formData = new FormData()
    formData.append('image', values.image.file.originFileObj)
    formData.append('name', values.categoryName)
    formData.append('description', values.description)

    await this.props.addHouseCategory({formData,  callback: (values) => {
      if(values) {
        this.resetForm()
        this.props.history.push('house-category')
      }
    }})
  };

  editHouseCategory = async (values) => {
    let formData = new FormData()
    formData.append('id', this.props.detailData.id)
    formData.append('name', values.categoryName)
    formData.append('description', values.description)
    let params = {
      description: values.description,
      name: values.categoryName,
      id: this.props.detailData.id
    }
    await this.props.updateHouseCategory({params, callback: (values) => {
      if(values) {
        this.props.history.push('/house-category')
        this.resetForm()
      }
    }})
  };

  handleChangeImage = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false,
        }),
      );
    }
  };

  backToHouseCategory = () => {
    this.props.history.push("/house-category")
  }

  resetForm = () => {
    this.formRef.current.resetFields();
    this.setState({
      imageUrl: null
    })
  }

  render() {
    const { detailData, type } = this.props
    const { loading, imageUrl } = this.state;
    const uploadButton = (
      <div>
        {loading ? <LoadingOutlined /> : <PlusOutlined />}
        <div style={{ marginTop: 8 }}>Upload</div>
      </div>
    );

    const layout = {
      labelCol: { span: 24 },
      wrapperCol: { span: 24 },
    };

    let initialValues
    if (detailData) {
      initialValues = {
        categoryName: detailData.name,
        description: detailData.description,
        image: detailData.imagePath
      }
    }
    return (
      <div>
        {this.props.processing && <Spinner/>}
        <Form
          {...layout}
          name="basic"
          ref={this.formRef}
          initialValues={initialValues}
          onFinish={type === 'edit' ? this.editHouseCategory : this.addHouseCategory}
        >
          <Form.Item
            label="Loại phòng"
            name="categoryName"
            rules={[{ required: true, message: 'Vui lòng nhập tên loại phòng!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Mô tả"
            name="description"
            rules={[{ required: true, message: 'Vui lòng nhập mô tả!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Ảnh"
            name="image"
            rules={[{ required: true, message: 'Vui lòng đăng ảnh!' }]}
            valuePropName=""
          >
            <Upload
              name="avatar"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
              beforeUpload={beforeUpload}
              onChange={this.handleChangeImage}
            >
              {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
            </Upload>
          </Form.Item>

          <Form.Item>
            <div className="d-flex-center-end">
              <Space size="middle">
                <Button className="btn-success" size="middle" type="primary" htmlType="submit">
                  {type === 'edit' ? 'Cập nhật' : 'Thêm'}
                </Button>
                <Button size="middle" type="danger" onClick={this.backToHouseCategory}>
                  Quay lại
                </Button>
              </Space>
            </div>
          </Form.Item>
        </Form>

      </div>
    );
  }
}

const mapStateToProps = state => {
  return {

    processing: state.category.processing
  }
}

const mapDispatchToProps = dispatch => ({
  addHouseCategory: (data) => dispatch(HouseCategoryActions.addHouseCategoryRequest(data)),
  updateHouseCategory: (data) => dispatch(HouseCategoryActions.updateHouseCategoryRequest(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(HouseCategoryForm))
