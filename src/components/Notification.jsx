import { notification } from 'antd';

const Notify = (type, message) => {
  notification[type]({
    message: 'Thông báo',
    description: message
  });
};

export default Notify