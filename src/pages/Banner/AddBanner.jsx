import React, { Component } from "react";
import { connect } from 'react-redux'
import { withRouter } from "react-router";
import BannerActions from '../../redux/_banner-redux'
import { Switch, Button, Form, Input, Upload, message, Space } from 'antd';
import { CloseOutlined, CheckOutlined, PlusOutlined, LoadingOutlined } from '@ant-design/icons';
import Spinner from '../../components/Spinner'

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}
class AddBanner extends Component {
  formRef = React.createRef();

  state = {
    loading: false,
    activeImage: false
  }

  addBanner = async (values) => {
    let formData = new FormData()
    formData.append('file', values.image.file.originFileObj)
    formData.append('name', values.imageName)
    formData.append('active', values.active)

    await this.props.addNewBanner({formData, callback: (values) => {
      if(values) {
        this.resetForm()
        this.props.history.push("/banner")
      }
    }})
  };

  resetForm = () => {
    this.formRef.current.resetFields();
    this.setState({
      imageUrl: null
    })
  }

  handleChangeImage = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false,
        }),
      );
    }
  };

  backToListBanner = () => {
    this.props.history.push("/banner")
  }

  render() {
    const { loading, imageUrl } = this.state;
    const uploadButton = (
      <div>
        {loading ? <LoadingOutlined /> : <PlusOutlined />}
        <div style={{ marginTop: 8 }}>Upload</div>
      </div>
    );

    const layout = {
      labelCol: { span: 24 },
      wrapperCol: { span: 24 },
    };
    return (
      <div className="add_banner">
        {this.props.processing && <Spinner/>}
        <div className="fs-22 fw-6">
          <p>Thêm mới banner</p>
        </div>
        <div className="form_container">
          <Form
            {...layout}
            name="basic"
            ref={this.formRef}
            initialValues={{ active: true , imageName: ''}}
            onFinish={this.addBanner}
          >
            <Form.Item
              label="Tên"
              name="imageName"
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Ảnh"
              name="image"
              rules={[{ required: true, message: 'Vui lòng đăng ảnh!' }]}
              valuePropName=""
            >
              <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                beforeUpload={beforeUpload}
                onChange={this.handleChangeImage}
              >
                {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
              </Upload>
            </Form.Item>
            <Form.Item
              label="Trạng thái"
              name="active"
              valuePropName="checked"
            >
              <Switch
                checkedChildren={<CheckOutlined />}
                unCheckedChildren={<CloseOutlined />}
                defaultChecked
              />
            </Form.Item>
            <Form.Item>
              <div className="d-flex-center-end">
                <Space size="middle">
                  <Button className="btn-success" size="middle" type="primary" htmlType="submit">
                    Thêm
                </Button>
                  <Button size="middle" type="danger" onClick={this.backToListBanner}>
                    Quay lại
                </Button>
                </Space>
              </div>
            </Form.Item>
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    processing: state.banner.processing
  }
}

const mapDispatchToProps = dispatch => ({
  addNewBanner: (data) => dispatch(BannerActions.createImageSlideRequest(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AddBanner))
