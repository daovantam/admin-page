import React, { Component } from "react";
import { connect } from 'react-redux'
import { withRouter } from "react-router";
import BannerActions from '../../redux/_banner-redux'
import { Table, Switch, Button, } from 'antd';
import { CloseOutlined, CheckOutlined, PlusOutlined } from '@ant-design/icons';
import { Helmet } from 'react-helmet'
import Spinner from '../../components/Spinner'

class ManageBanner extends Component {
  state = {
    visible: false,
    loading: false,
    active: true
  };

  componentDidMount() {
    this.props.getListImageSlide()
  }
  

  toAddBanner = () => {
    this.props.history.push('/add-banner')
  }

  changeActiveBanner = (record) => {
    console.log(record)
    let params = {
      id: record.id,
      status: !record.active
    }

    this.props.changeActiveSlide({params, callback: (values) => {
      if(values) {
        this.props.getListImageSlide()
      }
    }})
  }


  render() {
    const { listImageSlide } = this.props
    listImageSlide && listImageSlide.map((item, index) => {
      listImageSlide[index].key = item.id
    })
    const columns = [
      {
        title: 'Ảnh',
        dataIndex: 'bannerImage',
        key: 'bannerImage',
        render: (text, record) => (
          <img style={{ height: '100px', borderRadius: '4px' }} src={record.path} />
        ),
      },
      {
        title: 'Tên',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Thứ tự',
        dataIndex: 'order',
        key: 'order',
      },
      {
        title: 'Trạng thái',
        key: 'status',
        dataIndex: 'status',
        render: (text, record) => (
          <Switch
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
            checked={record.active}
            onChange={() => {
              this.changeActiveBanner(record)
            }}
          />
        ),
      },
    ];

    return (
      <div className="manage_banner">
        {/* {this.props.processing && <Spinner/>} */}
        <Helmet>
          <title>Danh sách banner</title>
        </Helmet>
        <div className="d-flex-space-between mb-2">
          <p className="fs-22 fw-6">Danh sách banner</p>
          <Button type="primary" onClick={this.toAddBanner} className="btn-success"><PlusOutlined />Thêm</Button>
        </div>
        <Table columns={columns} dataSource={listImageSlide} pagination={{ pageSize: 5 }} rowkey="id" />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    processing: state.banner.processing,
    listImageSlide: state.banner.listImageSlide
  }
}

const mapDispatchToProps = dispatch => ({
  getListImageSlide: () => dispatch(BannerActions.getImageSlideRequest()),
  changeActiveSlide: (data) => dispatch(BannerActions.changeActiveSlideRequest(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ManageBanner))