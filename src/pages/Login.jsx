import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from "react-router"
import AuthActions from '../redux/_auth-redux'
import { Form, Input, Button, Checkbox, Modal, message } from 'antd'
import { UserOutlined, LockOutlined, MailOutlined } from '@ant-design/icons'
import { Auth } from '../constants'

class Login extends Component {
  state = {
    loading: true,
    visibleForgotPassword: false
  };

  login = values => {
    console.log(values) 
    let formData = new FormData()
    formData.append('username', values.username)
    formData.append('password', values.password)
    formData.append('grant_type', Auth.GRANT_TYPE)
    console.log(Auth.GRANT_TYPE)
    this.props.login(formData)
  }

  componentDidUpdate(prevProps) {
    let loginSuccess = this.props.data?.loginSuccess
    if (loginSuccess) {
      this.props.history.push('/')
    }
  }

  openModalForgotPassword = () => {
    this.setState({
      visibleForgotPassword: true,
    });
  }

  handleForgotPassword = (values) => {
    console.log(values)
    this.props.resetPassword(values.username_forgot)
  };

  handleCancelForgotPassword = e => {
    console.log(e);
    this.setState({
      visibleForgotPassword: false,
    });
  };

  render() {
    console.log(this.props.data)
    return (
      <div className="login_container">
        <div className="content_wrapper">
          <div className="text-center mb-3">
            <p className="title">House Rental</p>
          </div>
          <p className="fs-16">Đăng nhập vào trang quản trị của House Rental</p>
          <Form
            name="normal_login"
            onFinish={this.login}
          >
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập tên người dùng!',
                },
              ]}
            >
              <Input size="large" prefix={<UserOutlined />} placeholder="Tên người dùng" />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập mật khẩu! ',
                },
              ]}
            >
              <Input.Password
                size="large"
                prefix={<LockOutlined />}
                type="password"
                placeholder="Mật khẩu"
              />
            </Form.Item>
            <Form.Item>
              <p onClick={this.openModalForgotPassword} className="text-end">
                Quên mật khẩu?
              </p>
            </Form.Item>

            <Form.Item >
              <Button size="large" type="primary" htmlType="submit" className="w-100">
                Đăng nhập
                </Button>
            </Form.Item>
          </Form>

        </div>

        {/* modal forgot password */}
        <Modal
          title="Nhập tên người dùng để lấy lại mật khẩu!"
          visible={this.state.visibleForgotPassword}
          // onOk={this.handleForgotPassword}
          onCancel={this.handleCancelForgotPassword}
          footer={null}
        >
          <Form
            name="normal_login"
            onFinish={this.handleForgotPassword}
          >
            <Form.Item
              name="username_forgot"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập tên người dùng!',
                },
              ]}
            >
              <Input size="large" prefix={<UserOutlined />} placeholder="Tên người dùng" />
            </Form.Item>
            <Form.Item>
              <div className="d-flex-center-end">
                <Button size="large" type="primary" htmlType="submit" style={{ width: '30%' }}>
                  Gửi
                  </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>

    )
  }
}

const mapStateToProps = state => {
  return {
    data: state.auth.data,
  }
}

const mapDispatchToProps = dispatch => ({
  login: (data) => dispatch(AuthActions.loginRequest(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Login))
