import React, { Component } from "react";
import { Table, Tag, Space } from 'antd';

class User extends Component {
  render() {
    const columns = [
      {
        title: 'Tên người dùng',
        dataIndex: 'userName',
        key: 'userName',
      },
      {
        title: 'Số điện thoại',
        dataIndex: 'phoneNumber',
        key: 'phoneNumber',
      },
      {
        title: 'Email',
        key: 'email',
        dataIndex: 'email',
      },
      {
        title: 'Ngày tạo',
        key: 'createdDate',
        dataIndex: 'createdDate',
      },
      {
        title: 'Vai trò',
        key: 'role',
        dataIndex: 'role',
      },
      {
        title: '',
        key: 'action',
        dataIndex: 'action',
        render: (text, record) => (
          <Space size="middle">
            <Tag color="green">Chỉnh sửa</Tag>
            <Tag color="red">Xoá</Tag>
          </Space>
        ),
      },
    ];

    const data = [
      {
        key: '1',
        userName: 'User',
        phoneNumber: '0123456789',
        email: 'user@domain.com',
        createdDate: '01-01-2020',
        role: 'Sale'
      },
      {
        key: '2',
        userName: 'User',
        phoneNumber: '0123456789',
        email: 'user@domain.com',
        createdDate: '01-01-2020',
        role: 'Sale'
      },
      {
        key: '3',
        userName: 'User',
        phoneNumber: '0123456789',
        email: 'user@domain.com',
        createdDate: '01-01-2020',
        role: 'Sale'
      },
      {
        key: '4',
        userName: 'User',
        phoneNumber: '0123456789',
        email: 'user@domain.com',
        createdDate: '01-01-2020',
        role: 'Sale'
      },
    ];
    return (
      <div>
        <div className="fs-22 fw-6">
          <p>Thông tin người dùng</p>
        </div>
        <Table columns={columns} dataSource={data} />
      </div>
    );
  }
}

export default User;