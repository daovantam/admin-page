import React, { Component } from "react";
import { connect } from 'react-redux'
import { withRouter } from "react-router";
import HouseActions from '../../redux/_house-redux'
import { Table, Tag, Space, Pagination, Popconfirm } from 'antd';
import { ClockCircleOutlined, TagsOutlined } from '@ant-design/icons';
import { Helmet } from 'react-helmet'
import Spinner from '../../components/Spinner'
class ManageHouse extends Component {
  state = {
    pageSize: 5,
  }

  componentDidMount() {
    let params = {
      direction: true,
      matching: [
        {
          key: "status",
          operation: ":",
          orPredicate: true,
          value: true
        }
      ],
      orderBy: "updatedTimestamp",
      page: 0,
      pageSize: this.state.pageSize
    }
    this.props.getListHouse(params)
  }

  deleteHouse = (record) => {
    let paramsGetList = {
      direction: true,
      matching: [
        {
          key: "status",
          operation: ":",
          orPredicate: true,
          value: true
        }
      ],
      orderBy: "updatedTimestamp",
      page: 0,
      pageSize: this.state.pageSize
    }
    let paramsDelete = {
      id: record.id
    };
    this.props.deleteHouse({
      paramsDelete, callback: (values) => {
        if (values) {
          this.props.getListHouse(paramsGetList)
        }
      }
    })
  }

  toHouseDetail = (record) => {
    let id = record.id
    this.props.getDetailHouse({
      id, callback: (values) => {
        if (values) {
          this.props.history.push(`house-detail/${record.id}`)
        }
      }
    })
  }

  onShowSizeChange = (current, pageSize) => {
    this.setState({
      pageSize: pageSize
    })
    let params = {
      direction: true,
      matching: [
        {
          key: "status",
          operation: ":",
          orPredicate: true,
          value: true
        }
      ],
      orderBy: "updatedTimestamp",
      page: 0,
      pageSize: this.state.pageSize
    }
    this.props.getListHouse(params)
  }

  onSelectPageChange = (page) => {
    let params = {
      direction: true,
      matching: [
        {
          key: "status",
          operation: ":",
          orPredicate: true,
          value: true
        }
      ],
      orderBy: "updatedTimestamp",
      page: page - 1,
      pageSize: this.state.pageSize
    }
    this.props.getListHouse(params)
    window.scrollTo(0, 0)
  };

  render() {
    const listHouse = this.props.data?.list_object
    const total_element = this.props.data?.total_element
    listHouse && listHouse.map((item, index) => {
      listHouse[index].key = item.id
    })
    const columns = [
      {
        title: 'Ảnh',
        dataIndex: 'houseImage',
        key: 'houseImage',
        render: (text, record) => (
          <img style={{ height: '100px', borderRadius: '4px' }} src={record.imagesPath[0]} />
        ),
      },
      {
        title: 'Tên',
        dataIndex: 'houseName',
        key: 'houseName',
        render: (text, record) => (
          <span>{record.title}</span>
        ),
      },
      {
        title: 'Loại',
        dataIndex: 'houseCategory',
        key: 'houseCategory',
        render: (text, record) => (
          <span><Tag icon={<TagsOutlined />} color="#2db7f5">{record.categoryName}</Tag></span>
        ),
      },
      {
        title: 'Địa chỉ',
        key: 'address',
        dataIndex: 'address',
        render: (text, record) => (
          <span>{record.wardName}, {record.districtName}</span>
        ),
      },
      {
        title: 'Trạng thái',
        key: 'status',
        dataIndex: 'status',
        render: (text, record) => (
          <div>
            {record.status ? (
              <Tag color="#87d068">Đang hoạt động</Tag>
            ) : (
                <Tag icon={<ClockCircleOutlined />} color="default">
                  Chờ phê duyệt
                </Tag>
              )}

          </div>
        ),
      },
      {
        title: '',
        key: 'action',
        dataIndex: 'action',
        render: (text, record) => (
          <Space size="">
            <Tag color="blue" className="cursor-pointer" onClick={() => { this.toHouseDetail(record) }}>Chi tiết</Tag>
            <Popconfirm
              title="Xoá phòng này?"
              onConfirm={() => { this.deleteHouse(record) }}
              onCancel={this.cancelDeleteHouse}
              okText="Yes"
              cancelText="No"
            >
              <Tag color="red" className="cursor-pointer" >Xoá</Tag>
            </Popconfirm>
          </Space>
        ),
      },
    ];

    return (
      <div>
        <Helmet>
          <title>Danh sách phòng</title>
        </Helmet>
        {/* {this.props.processing && <Spinner />} */}
        <div className="fs-22 fw-6">
          <p>Danh sách phòng</p>
        </div>
        <Table columns={columns} dataSource={listHouse} pagination={false} rowKey={record => record.id} />
        <div className="mt-3 d-flex-center-end">
          <Pagination
            onShowSizeChange={this.onShowSizeChange}
            onChange={this.onSelectPageChange}
            defaultCurrent={1} total={total_element} pageSize={this.state.pageSize}
            locale={{ items_per_page: '/ Trang' }}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    processing: state.house.processing,
    data: state.house.listHouse
  }
}

const mapDispatchToProps = dispatch => ({
  getListHouse: (data) => dispatch(HouseActions.getListHouseRequest(data)),
  deleteHouse: (data) => dispatch(HouseActions.deleteHouseRequest(data)),
  getDetailHouse: (data) => dispatch(HouseActions.getDetailHouseRequest(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ManageHouse))
