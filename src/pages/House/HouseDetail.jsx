import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from "react-router"
import HouseActions from '../../redux/_house-redux'
import { Descriptions, Upload, Space, Button } from 'antd'
import CKEditor from 'ckeditor4-react'
import AppUtil from '../../utils/appUtils'

class HouseDetail extends Component {
  state = {
    loading: true,
    visibleForgotPassword: false,
    fileList: []
  };

  componentDidMount() {
    let id = this.props.match.params.id
    if (id) {
      this.props.getDetailHouse({
        id, callback: (values) => {
          if (values) {
          }
        }
      })
    }
  }

  backToListHouse = () => {
    this.props.history.push('/house')
  }

  render() {
    const detailHouse = this.props?.detailHouse
    let fileList = []
    if (this.props.detailHouse.imagesPath) {
      this.props.detailHouse.imagesPath.map((item, index) => {
        fileList.push({
          uid: index,
          name: "",
          status: "done",
          url: item,
        })
      });
    }

    console.log(detailHouse.description)

    return (
      <div>
        <div className="fs-22 fw-6">
          <p>Thông tin chi tiết</p>
        </div>
        <div>
          <Descriptions bordered>
            <Descriptions.Item label="Tên" span={3}>{detailHouse.title}</Descriptions.Item>
            <Descriptions.Item label="Loại phòng" span={3}>{detailHouse.categoryName}</Descriptions.Item>
            <Descriptions.Item label="Giá tiền">{AppUtil.number_format(detailHouse.price)}₫ /{detailHouse.timeline}</Descriptions.Item>
            <Descriptions.Item label="Diện tích">{detailHouse.acreage}</Descriptions.Item>
            <Descriptions.Item label="Số phòng ngủ">{detailHouse.numberOfBedroom}</Descriptions.Item>
            <Descriptions.Item label="Mô tả" span={3}>
              <CKEditor data={detailHouse.description} />
            </Descriptions.Item>
            <Descriptions.Item label="Địa chỉ" span={3}>{detailHouse.wardName}, {detailHouse.districtName}, {detailHouse.provinceName}</Descriptions.Item>
            <Descriptions.Item label="Ảnh" span={3}>
              <Upload
                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                listType="picture-card"
                fileList={fileList}
                onPreview={this.handlePreview}
                onChange={this.handleChange}
                beforeUpload={() => false}
              >
              </Upload>
            </Descriptions.Item>
          </Descriptions>
        </div>
        <div className="d-flex-center-end mt-2">
        <Space size="middle">
          <Button size="middle" type="danger" onClick={this.backToListHouse}>
            Quay lại
          </Button>
        </Space>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    detailHouse: state.house.detailHouse,
  }
}

const mapDispatchToProps = dispatch => ({
  getDetailHouse: (data) => dispatch(HouseActions.getDetailHouseRequest(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(HouseDetail))
