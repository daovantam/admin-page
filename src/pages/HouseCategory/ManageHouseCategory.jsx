import React, { Component } from "react";
import { connect } from 'react-redux'
import { withRouter } from "react-router";
import HouseCategoryActions from '../../redux/_category-redux'
import { Table, Tag, Space, Button, Popconfirm } from 'antd';
import { PlusOutlined, TagsOutlined } from '@ant-design/icons';
import { AppUtils } from '../../utils'
import { Helmet } from 'react-helmet'
let moment = require("moment");
class ManageHouseCategory extends Component {
  componentDidMount() {
    this.props.getListHouseCategory()
  }


  toAddHouseCategory = () => {
    this.props.history.push('/add-house-category')
  }

  deleteCategory = (record) => {
    let params = {
      id: record.id
    };
    this.props.deleteHouseCategory({
      params, callback: (values) => {
        if (values) {
          this.props.getListHouseCategory()
        }
      }
    })
  }

  editHouseCategory = (record) => {
    let id = record.id
    this.props.getDetailCategory({id, callback: (values) => {
      if(values) {
        this.props.history.push(`/house-category/${record.id}`)
      }
    }})
  }
  render() {
    const { listHouseCategory } = this.props
    listHouseCategory && listHouseCategory.map((item, index) => {
      listHouseCategory[index].key = item.id
    })
    const columns = [
      {
        title: 'Ảnh',
        dataIndex: 'image',
        key: 'image',
        render: (text, record) => (
          <img style={{ height: '100px', borderRadius: '4px' }} src={record.imagePath} />
        ),
      },
      {
        title: 'Loại Phòng',
        dataIndex: 'category',
        key: 'category',
        render: (text, record) => (
          <span><Tag icon={<TagsOutlined />} color="#2db7f5">{record.name}</Tag></span>
        ),
      },
      {
        title: 'Mô tả',
        dataIndex: 'description',
        key: 'description',
        render: (text, record) => (
          <span>{record.description}</span>
        ),
      },
      {
        title: 'Người tạo',
        key: 'createdBy',
        dataIndex: 'createdBy',
        render: (text, record) => (
          <span>{record.updateBy}</span>
        ),
      },
      {
        title: 'Ngày tạo',
        key: 'createdDate',
        dataIndex: 'createdDate',
        render: (text, record) => (
        <span>{moment(record.updatedTimestamp).format("DD/MM/YYYY")}</span>
        ),
      },
      {
        title: '',
        key: 'action',
        dataIndex: 'action',
        render: (text, record) => (
          <Space size="">
            <Tag color="green" className="cursor-pointer" onClick={() => {this.editHouseCategory(record)}}>Chỉnh sửa</Tag>
            <Popconfirm
              title="Xoá loại phòng này?"
              onConfirm={() => { this.deleteCategory(record) }}
              onCancel={this.cancelDeleteCategory}
              okText="Yes"
              cancelText="No"
            >
              <Tag color="red" className="cursor-pointer" >Xoá</Tag>
            </Popconfirm>
          </Space>
        ),
      },
    ];

    return (
      <div className="house_category">
        <Helmet>
          <title>Danh sách loại phòng</title>
        </Helmet>
        <div className="d-flex-space-between mb-2">
          <p className="fs-22 fw-6">Quản lý loại phòng</p>
          <Button type="primary" className="btn-success" onClick={this.toAddHouseCategory}><PlusOutlined />Thêm</Button>
        </div>
        <Table columns={columns} dataSource={listHouseCategory} pagination={{ pageSize: 5 }} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    listHouseCategory: state.category.listHouseCategory
  }
}

const mapDispatchToProps = dispatch => ({
  getListHouseCategory: () => dispatch(HouseCategoryActions.getHouseCategoryRequest()),
  deleteHouseCategory: (data) => dispatch(HouseCategoryActions.deleteHouseCategoryRequest(data)),
  getDetailCategory: (data) => dispatch(HouseCategoryActions.getDetailCategoryRequest(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ManageHouseCategory))