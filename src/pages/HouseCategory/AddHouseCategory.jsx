import React, { Component } from "react";
import { connect } from 'react-redux'
import { withRouter } from "react-router";
import HouseCategoryActions from '../../redux/_category-redux'
import HouseCategoryform from '../../components/HouseCategoryForm'


class AddHouseCategory extends Component {
  state = {
    loading: false,
    activeImage: false
  }

  render() {
    return (
      <div className="add_banner">
        <div className="fs-22 fw-6">
          <p>Thêm mới loại phòng</p>
        </div>
        <div className="form_container">
          <HouseCategoryform type="create"/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    listImageSlide: state.banner.listImageSlide
  }
}

const mapDispatchToProps = dispatch => ({
  addHouseCategory: (data) => dispatch(HouseCategoryActions.addHouseCategoryRequest(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AddHouseCategory))
