import React, { Component } from "react";
import { connect } from 'react-redux'
import { withRouter } from "react-router";
import HouseCategoryActions from '../../redux/_category-redux'
import HouseCategoryform from '../../components/HouseCategoryForm'


class AddHouseCategory extends Component {
  state = {
    loading: false,
    activeImage: false,
    detailCategory: {}
  }

  componentDidMount = () => {
    let id = this.props.match.params.id
    if(id) {
      this.props.getDetailCategory({id, callback: (values) => {
        if(values) {}
      }})
    }
  }

  render() {
    const { detailCategory } = this.props
    return (
      <div className="add_banner">
        <div className="fs-22 fw-6">
          <p>Cập nhật thông tin loại phòng</p>
        </div>
        <div className="form_container">
          <HouseCategoryform detailData={detailCategory} type="edit"/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    detailCategory: state.category.detailCategory
  }
}

const mapDispatchToProps = dispatch => ({
  getDetailCategory: (data) => dispatch(HouseCategoryActions.getDetailCategoryRequest(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AddHouseCategory))
