import React, { Component } from "react";
import { connect } from 'react-redux'
import { withRouter } from "react-router";
import BannerActions from '../redux/_banner-redux'
import { Table, Switch, Button, } from 'antd';
import { CloseOutlined, CheckOutlined, PlusOutlined } from '@ant-design/icons';
import { Helmet } from 'react-helmet'

class NotFound extends Component {
  state = {
    visible: false,
    loading: false,
    active: true
  };

  componentDidMount() {
    this.props.getListImageSlide()
  }
  

  toAddBanner = () => {
    this.props.history.push('/add-banner')
  }

  changeActiveBanner = (record) => {
    console.log(record)
    let params = {
      id: record.id,
      status: !record.active
    }

    this.props.changeActiveSlide({params, callback: (values) => {
      if(values) {
        console.log("vao day")
        this.props.getListImageSlide()
      }
    }})
  }


  render() {
    const { listImageSlide } = this.props
    listImageSlide && listImageSlide.map((item, index) => {
      listImageSlide[index].key = item.id
    })
    const columns = [
      {
        title: 'Ảnh',
        dataIndex: 'bannerImage',
        key: 'bannerImage',
        render: (text, record) => (
          <img style={{ height: '100px' }} src={record.path} />
        ),
      },
      {
        title: 'Tên',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Thứ tự',
        dataIndex: 'order',
        key: 'order',
      },
      {
        title: 'Trạng thái',
        key: 'status',
        dataIndex: 'status',
        render: (text, record) => (
          <Switch
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
            checked={record.active}
            onChange={() => {
              this.changeActiveBanner(record)
            }}
          />
        ),
      },
    ];

    return (
      <div className="manage_banner">
        not fount
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    listImageSlide: state.banner.listImageSlide
  }
}

const mapDispatchToProps = dispatch => ({
  getListImageSlide: () => dispatch(BannerActions.getImageSlideRequest()),
  changeActiveSlide: (data) => dispatch(BannerActions.changeActiveSlideRequest(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(NotFound))