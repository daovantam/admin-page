export default class AppUtils {
  static convertStringToDate(date) {
    console.log(date)
    // if (date) {
    //   return new Date(date.replace(/(\d{2})-(\d{2})-(\d{4})/, '$2/$1/$3 07:00'))
    // } else {
    //   return null
    // }
  }

  static convertStringTimeToDate(date) {
    if (date) {
      return new Date(date.replace(/(\d{2})-(\d{2})-(\d{4})/, '$2/$1/$3'))
    } else {
      return null
    }
  }

  static number_format( num) {
    if (num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    } else return 0
  }
  
}