import { createReducer, createActions } from 'reduxsauce'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  getListHouseRequest: ['data'],
  getListHouseSuccess: ['data'],
  getListHouseFailure: ['error'],
  getDetailHouseRequest: ['data'],
  getDetailHouseSuccess: ['data'],
  getDetailHouseFailure: ['error'],
  deleteHouseRequest: ['data'],
  deleteHouseSuccess: ['data'],
  deleteHouseFailure: ['error'],
})

export const HouseTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = {
  processing: false,
  succeed: false,
  listHouse: [],
  detailHouse: {}
}

/* ------------- Reducers ------------- */
export const request = state => {
  return { ...state, processing: true }
}

export const success = (state, { data }) => {
  return { ...state, processing: false }
}

export const getListHouseSuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true, listHouse: data.data }
}

export const getDetailHouseSuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true, detailHouse: data.data }
}


export const failure = (state) => {
  return { ...state, processing: false }
}

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_LIST_HOUSE_REQUEST]: request,
  [Types.GET_LIST_HOUSE_SUCCESS]: getListHouseSuccess,
  [Types.GET_LIST_HOUSE_FAILURE]: failure,
  [Types.GET_DETAIL_HOUSE_REQUEST]: request,
  [Types.GET_DETAIL_HOUSE_SUCCESS]: getDetailHouseSuccess,
  [Types.GET_DETAIL_HOUSE_FAILURE]: failure,
  [Types.DELETE_HOUSE_REQUEST]: request,
  [Types.DELETE_HOUSE_SUCCESS]: success,
  [Types.DELETE_HOUSE_FAILURE]: failure,
})
