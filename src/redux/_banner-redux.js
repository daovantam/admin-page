import { createReducer, createActions } from 'reduxsauce'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  getImageSlideRequest: ['data'],
  getImageSlideSuccess: ['data'],
  getImageSlideFailure: ['error', 'status'],
  createImageSlideRequest: ['data'],
  createImageSlideSuccess: ['data'],
  createImageSlideFailure: ['error', 'status'],
  changeActiveSlideRequest: ['data'],
  changeActiveSlideSuccess: ['data'],
  changeActiveSlideFailure: ['error', 'status']
})

export const BannerTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = {
  processing: false,
  succeed: false,
  listImageSlide: []
}

/* ------------- Reducers ------------- */
export const request = state => {
  return { ...state, processing: true }
}

export const getImageSlideSuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true, listImageSlide: data.data }
}

export const createImageSlideSuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true }
}

export const changeActiveSlideSuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true }
}


export const failure = (state) => {
  return { ...state, processing: false }
}

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_IMAGE_SLIDE_REQUEST]: request,
  [Types.GET_IMAGE_SLIDE_SUCCESS]: getImageSlideSuccess,
  [Types.GET_IMAGE_SLIDE_FAILURE]: failure,
  [Types.CREATE_IMAGE_SLIDE_REQUEST]: request,
  [Types.CREATE_IMAGE_SLIDE_SUCCESS]: createImageSlideSuccess,
  [Types.CREATE_IMAGE_SLIDE_FAILURE]: failure,
  [Types.CHANGE_ACTIVE_SLIDE_REQUEST]: request,
  [Types.CHANGE_ACTIVE_SLIDE_SUCCESS]: changeActiveSlideSuccess,
  [Types.CHANGE_ACTIVE_SLIDE_FAILURE]: failure
})
