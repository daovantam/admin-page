import { createReducer, createActions } from 'reduxsauce'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  loginRequest: ['data'],
  loginSuccess: ['data'],
  loginFailure: ['error'],
  getUserInfoRequest: ['data'],
  getUserInfoSuccess: ['data'],
  getUserInfoFailure: ['error'],

})

export const AuthTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = {
  processing: false,
  succeed: false, 
  data: {},
  userInfo: {}
}

/* ------------- Reducers ------------- */
export const request = state => {
  return { ...state, processing: true }
}

export const success = (state, { data }) => {
  return { ...state, processing: false, succeed: true, data: data }
}

export const getUserInfoSuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true, userInfo: data }
}


export const failure = (state) => {
  return { ...state, processing: false }
}

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_REQUEST]: request,
  [Types.LOGIN_SUCCESS]: success,
  [Types.LOGIN_FAILURE]: failure,
  [Types.GET_USER_INFO_REQUEST]: request,
  [Types.GET_USER_INFO_SUCCESS]: getUserInfoSuccess,
  [Types.GET_USER_INFO_FAILURE]: failure,
})
