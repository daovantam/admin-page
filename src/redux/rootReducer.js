import { combineReducers } from "redux";
import { reducer as modal } from "redux-modal";

const rootReducer = combineReducers({
  modal,
  banner: require('./_banner-redux').reducer,
  category: require('./_category-redux').reducer,
  house: require('./_house-redux').reducer,
  auth: require('./_auth-redux').reducer,
});


export default rootReducer;
