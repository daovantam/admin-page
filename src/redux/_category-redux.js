import { createReducer, createActions } from 'reduxsauce'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  getHouseCategoryRequest: ['data'],
  getHouseCategorySuccess: ['data'],
  getHouseCategoryFailure: ['error'],

  getDetailCategoryRequest: ['data'],
  getDetailCategorySuccess: ['data'],
  getDetailCategoryFailure: ['error'],

  addHouseCategoryRequest: ['data'],
  addHouseCategorySuccess: ['data'],
  addHouseCategoryFailure: ['error'],

  deleteHouseCategoryRequest: ['data'],
  deleteHouseCategorySuccess: ['data'],
  deleteHouseCategoryFailure: ['error'],
  
  updateHouseCategoryRequest: ['data'],
  updateHouseCategorySuccess: ['data'],
  updateHouseCategoryFailure: ['error'],
})

export const HouseCategoryTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = {
  processing: false,
  succeed: false,
  listHouseCategory: [],
  detailCategory: {}
}

/* ------------- Reducers ------------- */
export const request = state => {
  return { ...state, processing: true }
}

export const success = (state, { data }) => {
  return { ...state, processing: false }
}

export const getHouseCategorySuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true, listHouseCategory: data.data }
}

export const getDetailCategorySuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true, detailCategory: data.data }
}

export const failure = (state, {data}) => {
  console.log(data)
  return { ...state, processing: false }
}

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_HOUSE_CATEGORY_REQUEST]: request,
  [Types.GET_HOUSE_CATEGORY_SUCCESS]: getHouseCategorySuccess,
  [Types.GET_HOUSE_CATEGORY_FAILURE]: failure,
  [Types.GET_DETAIL_CATEGORY_REQUEST]: request,
  [Types.GET_DETAIL_CATEGORY_SUCCESS]: getDetailCategorySuccess,
  [Types.GET_DETAIL_CATEGORY_FAILURE]: failure,
  [Types.ADD_HOUSE_CATEGORY_REQUEST]: request,
  [Types.ADD_HOUSE_CATEGORY_SUCCESS]: success,
  [Types.ADD_HOUSE_CATEGORY_FAILURE]: failure,
  [Types.DELETE_HOUSE_CATEGORY_REQUEST]: request,
  [Types.DELETE_HOUSE_CATEGORY_SUCCESS]: success,
  [Types.DELETE_HOUSE_CATEGORY_FAILURE]: failure,
  [Types.UPDATE_HOUSE_CATEGORY_REQUEST]: request,
  [Types.UPDATE_HOUSE_CATEGORY_SUCCESS]: success,
  [Types.UPDATE_HOUSE_CATEGORY_FAILURE]: failure,
})
