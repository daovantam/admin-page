import axios from 'axios';

const http = axios.create ({
  baseURL: 'http://27.72.88.246:8686',
  timeout: 10000,
  headers: {'Content-Type': 'application/json'},
});

// http.interceptors.request.use (
//   // function (config) {
//   //   const token = store.token;
//   //   if (token) config.headers.Authorization = `Bearer ${token}`;
//   //   return config;
//   // },
//   function (error) {
//     console.log(error)
//     return Promise.reject (error);
//   }
// );

export default http;